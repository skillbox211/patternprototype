#include <iostream>

/////////////////////////////////////////////////////////////////////////////////////////
//// VikBo - pattern Prototype
/////////////////////////////////////////////////////////////////////////////////////////


enum class EColor{
    RED,
    WHITE,
    BLACK
};

// Interface for custom cloning object
class ICloneInterface
{
public:
    virtual ICloneInterface* Clone() = 0;
};

// example class for cloning
class Ball: public ICloneInterface
{
private:
    int Radius{0};
    int Weight{0};
    EColor Color{EColor::WHITE};

public:
    Ball() = default;
    Ball(int _radius, int _weight, EColor _color): Radius(_radius), Weight(_weight), Color(_color) {};
    ~Ball() = default;

    ICloneInterface * Clone() override {
        Ball* NewBall  = new Ball();
        if (NewBall)
        {
            NewBall->Radius = Radius;
            NewBall->Weight = Weight;
        }
        return NewBall;
    }

    friend std::ostream& operator<<(std::ostream& out, const Ball& MyBall)
    {
        out << "Radius=" << MyBall.Radius << std::endl;
        out << "Weight=" << MyBall.Weight << std::endl;
        switch (MyBall.Color) {
            case EColor::RED:
                out << "Color=RED" << std::endl;
                break;
            case EColor::WHITE:
                out << "Color=WHITE" << std::endl;
                break;
            case EColor::BLACK:
                out << "Color=BLACK" << std::endl;
                break;
            default:
                break;
        }
        out << std::endl;
        return out;
    }
};


int main() {

    Ball MyFirstBall = Ball(10, 100, EColor::RED);
    std::cout << "# MyFirstBall\n";
    std::cout << MyFirstBall;
    Ball* PtrToMySecondBall = static_cast<Ball*>(MyFirstBall.Clone());
    std::cout << "# Not deep copy of MyFirstBall\n";
    std::cout << *PtrToMySecondBall;

    delete PtrToMySecondBall;

    return 0;
}
